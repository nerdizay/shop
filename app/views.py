from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from rest_framework.views import APIView
from .serializers import AuthSerializer


# class IndexView(APIView):
#     def get(self, request, *args, **kwargs):
#         return JsonResponse({"hello": "world"})


class LoginView(APIView):
    def post(self, request, *args, **kwargs):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is None:
            return JsonResponse({"detail": "not authorized"}, status=401)
        
        login(request, user)
        return JsonResponse({"detail": "success"})
        
    def get_serializer_class(self):
        return AuthSerializer