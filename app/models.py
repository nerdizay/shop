from typing import Collection
from django.db import models
from django.forms import ValidationError
from django.contrib.auth.models import User
from icecream import ic


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    parent = models.ForeignKey("self", on_delete=models.PROTECT, null=True, blank=True, verbose_name="Родитель")
    
    def __str__(self):
        if self.parent is None:
            return self.name
        
        return f"{str(self.parent)} > {self.name}"
    
    def get_tree_ids(self):
        if self.parent is None:
            return []
        
        return self.parent.get_tree_ids() + [self.parent.id]
    
    def clean(self):
        if self.id is not None and self.id in self.get_tree_ids():
            raise ValidationError("Нельзя выбрать категорию саму в себя")
        super().clean()
    
    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
    

class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    price = models.DecimalField(max_digits=8, decimal_places=2, verbose_name="Цена")
    sales_price = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True, verbose_name="Цена со скидкой")
    count = models.PositiveIntegerField(verbose_name="Количество")
    description = models.TextField(verbose_name="Описание")
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, verbose_name="Категория")
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
        
class Basket(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Продукт")
    count = models.PositiveIntegerField(verbose_name="Количество")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь")
    
    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"
        
    def __str__(self):
        return f"корзина пользователя {self.user}"
    
    def clean(self):
        ic(dir(self))
