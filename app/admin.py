from typing import Any
from django.contrib import admin
from django.http import HttpRequest
from .models import Product, Category, Basket
from icecream import ic


class ProductAdmin(admin.ModelAdmin):
    def has_change_permission(self, request: HttpRequest, obj = None) -> bool:
        return True


class CategoryAdmin(admin.ModelAdmin):
    def has_change_permission(self, request: HttpRequest, obj = None) -> bool:
        return True


class BasketAdmin(admin.ModelAdmin):
    list_display = ('user', 'product', 'count')
    readonly_fields = ('user',)
    
    def has_change_permission(self, request: HttpRequest, obj = None) -> bool:
        ic('Отработал метод')
        # if '/change/' not in request.path:
        #     return True
        # return True
        
        if '/admin/app/basket/' in request.path and '/change/' in request.path:
            obj_id = self.extract_id_from_change_request(request)
            if Basket.objects.get(id=obj_id).user == request.user:
                return True
            else:
                return False
        
        return True
    
    def save_model(self, request: Any, obj: Any, form: Any, change) -> None:
        obj.user = request.user
        return super(BasketAdmin, self).save_model(request, obj, form, change)
    
    def extract_id_from_change_request(self, request: HttpRequest) -> int:
        return int(request.path.replace('/admin/app/basket/', '').replace('/change/', ''))
    
    

admin.site.register(Product, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
admin.site.register(Basket, BasketAdmin)


admin.site.site_header = "Админ панель"

