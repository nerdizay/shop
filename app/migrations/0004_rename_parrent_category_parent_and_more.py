# Generated by Django 5.0.6 on 2024-05-09 11:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_alter_category_path'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='parrent',
            new_name='parent',
        ),
        migrations.RemoveField(
            model_name='category',
            name='number',
        ),
        migrations.RemoveField(
            model_name='category',
            name='path',
        ),
    ]
