from django.urls import path
from .views import (
    LoginView,
    # IndexView,
)


urlpatterns = [
    # path('', IndexView.as_view()),
    path('login', LoginView.as_view())
]
