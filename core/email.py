from django.core import mail

def send_email(from_='nerdizay@gmail.com', to='user@gmail.com', subject='Тема письма', body="Привет"):
    mail.EmailMessage(
        subject,
        body,
        from_,
        [to],
    ).send()
